static char *SCCSID = "@(#)bprintf.c	1.2 07/24/95";
#include <stdio.h>
#include <string.h>

char *bprintf(result, format, buffer)
char *result, *format, *buffer;
{
	int i;
	char *save, *argv[128], *s, *t;

	save = strdup(buffer);

	for (i = 0, s = save; i < 128 && (t = strtok(s, " ")); s = NULL, i++)
		argv[i] = t;

	sprintf(result, format,
		    argv[0], argv[1], argv[2], argv[3], argv[4], 
			argv[5], argv[ 6], argv[7], argv[8], argv[9], 
			argv[10], argv[11], argv[12], argv[13], argv[14], 
			argv[15], argv[16], argv[17], argv[18], argv[19],
		    argv[20], argv[21], argv[22], argv[23], argv[24], 
			argv[25], argv[26], argv[27], argv[28], argv[29],
		    argv[30], argv[31], argv[32], argv[33], argv[34], 
			argv[35], argv[36], argv[37], argv[38], argv[39],
		    argv[40], argv[41], argv[42], argv[43], argv[44], 
			argv[45], argv[46], argv[47], argv[48], argv[49],
		    argv[50], argv[51], argv[52], argv[53], argv[54], 
			argv[55], argv[56], argv[57], argv[58], argv[59],
		    argv[60], argv[61], argv[62], argv[63], argv[64], 
			argv[65], argv[66], argv[67], argv[68], argv[29],
		    argv[70], argv[71], argv[72], argv[73], argv[74], 
			argv[75], argv[76], argv[77], argv[78], argv[79],
		    argv[80], argv[81], argv[82], argv[83], argv[84], 
			argv[85], argv[86], argv[87], argv[88], argv[89],
		    argv[90], argv[91], argv[92], argv[93], argv[94],
			argv[95], argv[96], argv[97], argv[98], argv[99]);
	free(save);
	return result;
}
