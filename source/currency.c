#include <string.h>
#include <locale.h>
#include <math.h>

char *currency(double dval)
{
	char *dp, *cp, string[64];
	static char sval[2][64];
	static struct lconv *lconv = NULL;
	double dstep;
	int i;

	if (!lconv)
		lconv = localeconv();

	sprintf(string,"%.2f", dval);

	for (dp = sval[0], i = 0,cp = (strchr(string, '.') - 1); cp >= string; cp--, i++) {
		if (i % 3 == 0 && i > 0 && *cp != '-')
			*(dp++) = ',';
		*(dp++) = *cp;
	}
	*(dp--) = 0;

	for (cp = sval[1]; *cp = *dp; cp++, dp--);

	sprintf(sval[0], "$%s%s", sval[1], strchr(string, '.'));

	return sval[0];
}
