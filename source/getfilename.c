static char *SCCSID = "@(#)getfilename.c	1.2 07/24/95";
#include <limits.h>
#include <string.h>

char *getfilename(char *path)
{
	char *cp;
	static char filename[PATH_MAX];

	strcpy(filename, path);

	for (;;) {
		if (cp = strrchr(filename, '/')) {
			if (*(cp + 1)) {
				cp++;
				break;
			}
			else
				*cp = 0;
		}
		else {
			cp = filename;
			break;
		}
	}

	return cp;
}
