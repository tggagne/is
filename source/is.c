#include <stdlib.h>
#include <malloc.h>
#include "is.h"
// #include "isdio.h"

int
main(int argc, char **argv, char **envp)
{
	int i, err = 0;
	struct isHistoryNode *node;

	isData.isMsgQueue = QueueAlloc(1);
/*
isDefineSpace must be setup before the call
to isOptions because symbols may be installed
on the command line
*/
	isData.isDefineSpace = tabAlloc(10);
	isData.ColFmts = tabAlloc(10);

	if (isOptions(isData.argc = argc, isData.argv = argv)) {
		isData.isHistory = QueueAlloc(1);
		node = (struct isHistoryNode *) calloc(HISTORYNODES, sizeof(struct isHistoryNode));
		for (i = 0; i < HISTORYNODES; i++)
			InsertTail(isData.isHistory, (Node *) & node[i]);
/*
I've moved these two steps up since it makes little sense
to get the password before discovering we can't initialize
the Sybase libraries.
*/
		dbsetversion(isData.dbVersion);
		dbinit();

		isPassword();
		dbmsghandle(isMsgHandler);
		dberrhandle(isErrHandler);
		if (isDBOpen(isData.Server, isData.InitDataBase[0] ? isData.InitDataBase : NULL)) {
			isDBInterrupts(isData.dbproc);
			isPrompt("'$SERVER:$DB $BATCH.$LINE> '");
		}
		else
			err = -1;

		if (err == 0) {
			isInterrupts();
			isSecret();
			isData.input = malloc(INPUTSIZE);
			isLoop();
			free(isData.input);
			if (isData.html && !isData.vertical)
				isFPrintf("%s", "</BODY></HTML>\n");
		}
	}
	else
		isHelp();

	exit(EXIT_SUCCESS);
}
