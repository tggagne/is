#ifndef IS_H
#define IS_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/param.h>
#include "sybfront.h"
#include "sybdb.h"
#include "cspublic.h"
#include "queues.h"
#include "tabfuncs.h"

#ifdef HAVE_CONFIG_H
	#include "config.h"
#endif

extern int isErrHandler();
extern int isMsgHandler();

#ifndef DBMAXNAME
#define DBMAXNAME 16
#endif

struct {
	int busy;
	int batch;
	int sequence;
	DBPROCESS *dbproc;
	LOGINREC *login;
	char UserName[DBMAXNAME];
	char Password[DBMAXNAME];
	char AppName[DBMAXNAME];
	char Server[DBMAXNAME];
	char InitDataBase[DBMAXNAME];
	int interrupted;
	int last_signal;
	int columns;
	int headers;
	int retstatus;
	FILE *out;
	Queue *InQueue;
	tabSpace *isDefineSpace;
	Queue *isHistory;
	int echo;
	int argc;
	char **argv;
	char *input;
	char *dbname;
	char *prompt;
	char *history;
	char format[1024];
	short alarm_seconds;
	short rowcount;
	short html;
	short csv;
	short PassProvided;
	Queue *isMsgQueue;
	short suppress;
	short cookie;
	short padding;
	short border;
	short spacing;
	short vertical;
	tabSpace *ColFmts;
	short wholemoney;
	short width;
	short xml;
	char *serviceName;
	int	dbVersion;
	Queue *isIncludeDirectory;
} isData;

#define INPUTSIZE 2048
#define HISTORYNODES 100

struct isHistoryNode {
	Node node;
	char *command;
};

struct isInNode {
	struct isInNode *next;
	struct isInNode *previous;
	struct isInQueue *queue;
	FILE *in;
};

typedef struct {
	Node node;
	char *text;
} isMsgText;

#define DONOTHING 0
#define DOERROR 1
#define DOEXECUTE 2
#define DOOPTION 3
#define DOSYNTAX 4
#define DOOK 5

extern int isBang();
extern int isFile();
extern int isFPrintf(char *control, ...);
extern int isPrintf(char *control, ...);
extern FILE *isPOut();
extern void isHistList();
extern char *isResultsConvert();
extern char *isResultsFormat();
extern int isConnect(DBPROCESS *, int, char **);

#endif
