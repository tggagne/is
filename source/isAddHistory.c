static char *SCCSID = "@(#)isAddHistory.c	1.2 07/24/95";
#include <stdlib.h>
#include <string.h>
#include "is.h"

int isAddHistory(command)
char *command;
{
	struct isHistoryNode *node;

	node = (struct isHistoryNode *) RemoveHead(isData.isHistory);

	if (node->command)
		free(node->command);

	node->command = strdup(command);

	InsertTail(isData.isHistory, (Node *) node);
}
