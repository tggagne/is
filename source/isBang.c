static char *SCCSID = "@(#)isBang.c	1.2 07/24/95";
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "is.h"

int isBang(dbproc, b, s, command)
DBPROCESS *dbproc;
int *b;
int *s;
char *command;
{
	struct isHistoryNode *node;
	char *bang;
	int retval = DONOTHING;

	if (bang = strchr(command, '!')) {
		if (*(bang + 1) == '!') {
			if (*b > 1) {
				isBangInsert(dbproc, b, s, ((struct isHistoryNode *) isData.isHistory->tail)->command);
				retval = DOEXECUTE;
			} else {
				puts("\nNothing to repeat.");
				(*s)--;
			}
		} else if (*(bang + 1) == '-') {
			int back, i;
			back = atoi(bang + 2);
			if (back < HISTORYNODES - 1) {
				for (i = 0, node = (struct isHistoryNode *) isData.isHistory->tail; i < back - 1; i++, node = (struct isHistoryNode *) node->node.previous);
				isBangInsert(dbproc, b, s, node->command);
			} else {
				puts("\nCan't go back that far.");
				(*s)--;
			}
		} else if (isdigit(*(bang + 1))) {
			int back, i;
			back = atoi(bang + 1);
			if (back != 0) {
				if (back < *b) {
					for (i = *b - 1, node = (struct isHistoryNode *) isData.isHistory->tail;
					     node != (struct isHistoryNode *) QNULL && i > back; i--, node = (struct isHistoryNode *) node->node.previous);
					if (node != (struct isHistoryNode *) QNULL)
						isBangInsert(dbproc, b, s, node->command);
					else {
						(*s)--;
						puts("\nCan't go back that far.");
					}
				} else {
					puts("\nCan't find commands that haven't happened yet!");
					(*s)--;
				}
			} else {
				puts("\nThere was no command 0.");
				(*s)--;
			}
		} else if (isalpha(*(bang + 1))) {
			char template[128];
			sscanf(bang + 1, "%s", template);
			for (node = (struct isHistoryNode *) isData.isHistory->tail; node != (struct isHistoryNode *) QNULL; node = (struct isHistoryNode *) node->node.previous) {
				if (node->command) {
					if (memcmp(node->command, template, strlen(template)) == 0) {
						isBangInsert(dbproc, b, s, node->command);
						break;
					}
				}
			}
			if (node == (struct isHistoryNode *) QNULL)
				(*s)--;
		} else {
			puts("\nI don't know that one yet.");
			(*s)--;
		}
	}
	return retval;
}
