static char *SCCSID = "@(#)isBangInsert.c	1.2 07/24/95";
#include <stdlib.h>
#include <string.h>
#include "is.h"

int isBangInsert(dbproc, b, s, command)
DBPROCESS *dbproc;
int *b;
int *s;
char *command;
{
	struct isInNode *node;
	char tokenized[INPUTSIZE];
	char pstring[128];
	char *p1, *p2;

	if (*s == 1) {
		isInitHistory();
	}
	node = (struct isInNode *) isData.InQueue->tail;

	isCatHistory(command);

	for (p1 = command; *p1; p1 = p2 + 1) {
		p2 = strchr(p1, '\n');
		*p2 = 0;
		if (isatty(fileno(node->in)) || isData.echo) {
			isTokens(isData.prompt, pstring);
			isPrintf("%s%s\n", pstring, p1);
		}
		(*s)++;
		*p2 = '\n';
	}
	*s -= 1;
}
