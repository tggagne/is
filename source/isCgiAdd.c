static char *SCCSID = "@(#)isCgiAdd.c	1.6 08/22/96";
#include <string.h>
#include <malloc.h>
#include "is.h"

int isCgiAdd(DBPROCESS * dbproc, int *b, int *s, char *command)
{
	char *tok, *newtok = NULL, *cp;
	int c, retval = DOOK, parms = 0;

	command = strchr(command, ' ') + 1;
	for (; tok = strtok(command, "&"); command = NULL, isCatHistory(" ")) {
		if (strncmp(tok, "exec=", 5) == 0) {
			tok[4] = ' ';
		}
		else if (strncmp(tok, "isql=", 5) == 0)
			memmove(tok, tok + 5, strlen(tok + 5) + 1);

		while (cp = strchr(tok, '+'))
			*cp = ' ';
		
		while (cp = strchr(tok, '%')) {
			sscanf(cp + 1, "%2x", &c);
			*cp = c;
			memmove(cp + 1, cp + 3, strlen(cp + 3) + 1);
		}
/*
deal with parameters
*/
		if (tok[0] == '@') {
/*
if this is an argument, make sure we follow it 
with a comma
*/
			if (parms)
				isCatHistory(",");
			parms = 1;
/*
handle string arguments
*/
			cp = strchr(tok, '=');
			if (cp) {
				if (isalpha(*++cp)) {
/*
since we're making the string larger, we need to do it in another
part of memory, else we'd screw-up our command.
*/
					newtok = malloc(strlen(tok) + 1024); 	/* plenty of room -- I hope */
					strcpy(newtok, tok);
					cp = strchr(newtok, '=') + 1;
					memmove(cp + 1, cp, strlen(cp) + 1);
					*cp = '"';
					if (cp = strchr(newtok, '\n'))
						*cp = 0;
					strcat(tok = newtok, "\"");
				}
/*
sometimes, empty values will be null
*/
				else if (*cp == NULL || *cp == '\n') {
					newtok = malloc(strlen(tok) + 1024); 	/* plenty of room -- I hope */
					strcpy(newtok, tok);
					cp = strchr(newtok, '=');
					strcpy(++cp, "NULL");
					tok = newtok;
				}
			}
		}
		else
			parms = 0;

		isCatHistory(tok);

		if (newtok) {
			free(newtok);
			newtok = NULL;
		}
	}
}
