static char *SCCSID = "@(#)isCmdbuf.c	1.2 07/24/95";
#include <stdlib.h>
#include <string.h>
#include "is.h"

void isCmdbuf()
{
	int len;
	char *sol;
	char pstring[128];

	for (isData.sequence = 1, sol = isData.history; *sol != 0; isData.sequence++, sol += len) {
		len = strcspn(sol, "\n") + 1;
		isTokens(isData.prompt, pstring);
		isPrintf("%s%.*s", pstring, len, sol);
	}
	isData.sequence--;
}
