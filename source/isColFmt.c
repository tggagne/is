/* @(#)isColFmt.c	1.2 09/16/96 */
#include <stdlib.h>
#include <string.h>
#include "is.h"

int isColFmt(char *command)
{
	int converts;
	tabEntry *te;
	char symbol[128], value[128];
	struct isDefineNode *node;

	if ((converts = sscanf(command, ":colfmt %[^=]%*c%[^\n]", symbol, value)) > 0) {
		if (converts == 1)
			*value = 0;

		if (*value == '^')
			if ((te = tabLookup(isData.ColFmts, value + 1)) != QNULL)
				strcpy(value, te->def);

		tabInstall(isData.ColFmts, symbol, value);
	}
	else
		isDefList(isData.ColFmts);

	return 0;
}
