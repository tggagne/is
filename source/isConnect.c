#include "is.h"

int isConnect(DBPROCESS *dbproc, int argc, char **argv)
{
	int retval = DOSYNTAX;

	if (argc >= 2) {

		if (isDBOpen(argv[1], argc == 3 ? argv[2] : NULL) == TRUE) {
			strcpy(isData.Server, argv[1]);
			if (argc == 3)
				strcpy(isData.InitDataBase, argv[2]);
			dbclose(dbproc);
			retval = DOOK;
		}
		else {
			isData.dbproc = dbproc;
			retval = DOOPTION;
		}
	}
	return retval;
}
