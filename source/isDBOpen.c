static char *SCCSID = "@(#)isDBOpen.c	1.2 07/24/95";
#include <stdio.h>
#include <stdlib.h>
#include "is.h"

int isDBOpen(char *server, char *dbname)
{
	int err = FALSE;
	char width[10], *cp;

	if (isData.login = dblogin()) {
		if ((isData.UserName[0] == 0) && (cp = getenv("SYBUSER")))
			strcpy(isData.UserName, cp);
		DBSETLUSER(isData.login, isData.UserName);
		DBSETLPWD(isData.login, isData.Password);
		DBSETLAPP(isData.login, isData.AppName);
		if (isData.dbproc = dbopen(isData.login, server)) {
			isLineLen(isData.dbproc, 2047);	// seems like a reasonable number -- I'd rather the screen wrap
			err = TRUE;
			if (dbname)
				if (dbuse(isData.dbproc, dbname) == FAIL)
					err = isatty(fileno(((struct isInNode *) isData.InQueue->tail)->in)) ? TRUE : FALSE;
		}
	}
	return err;
}
