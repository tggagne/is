static char *SCCSID = "@(#)isDefList.c	1.3 09/16/96";
#include <stdlib.h>
#include <string.h>
#include "is.h"

int isDefList(tabSpace *ts)
{
	tabEntry *node;

	int i;

	for (i = 0; i < ts[0].hashsize; i++)
		for (node = (tabEntry *) ts[i].q.head; node != (tabEntry *) QNULL; node = (tabEntry *) node->n.next)
			isPrintf("%s=%s\n", node->name, node->def);
}
