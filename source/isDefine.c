/*
09/22/2005	tgagne
			free(value);
			*value = 0;
			I'm lucky it didn't segfault like it should have.
*/

#include <stdlib.h>
#include <string.h>
#include "is.h"

static char *SccsId = "@(#)isDefine.c	1.5 21 Jul 1994";

int isDefine(char *command, int argc, char **argv)
{
	int converts, len, equals;
	char *symbol, *value;
	struct isDefineNode *node;

/*
There's really no telling how big the symbol or the value
are going to be, so let's find out where the first '=' is
and allocate what we need.
*/
	len = strlen(command);
	equals = strcspn(command, "=");

	if (equals < len) {
		symbol = malloc(equals);
		value = malloc(len - equals);
		if ( (converts = sscanf(command, ":define %[^ =]%*c%[^\n]", symbol, value)) > 0) {

			if (converts == 1)
				*value = 0;	// make sure the string is null terminated

			else if (converts == 2) {
				if (strcmp(value, "<<") == 0) {
					value = realloc(value, 256);
					fgets(value, 255, ((struct isInNode *) isData.InQueue->tail)->in);
				}	
			}
			tabInstall(isData.isDefineSpace, symbol, value);
		}
	}
	else
		isDefList(isData.isDefineSpace);

	return 0;
}
