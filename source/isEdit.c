static char *SCCSID = "@(#)isEdit.c	1.3 08/12/96";
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "is.h"

#ifndef tmpnam
extern char *tmpnam(char *);
#endif

int isEdit(DBPROCESS *dbproc, int *b, int *s, char *command)
{
	int rc;
	FILE *fp;
	char *editor, string[256];
	char filename[10];

	strcpy(filename, "is.XXXXXX");

	if (fp = fdopen(mkstemp(filename), "w")) {
		fwrite(isData.history, strlen(isData.history), 1, fp);
		fclose(fp);
		editor = strtok(command, " \n");
		sprintf(string, "%s %s", editor, filename);
		rc = system(string);
		sprintf(string, ":r %s", filename);
		*s = 1;
		isSpecial(dbproc, b, s, string);
		unlink(filename);
	}
}
