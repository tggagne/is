static char *SCCSID = "@(#)isErrHandler.c	1.2 07/24/95";
#include "is.h"

int isErrHandler(dbproc, severity, dberr, oserr, dberrstr, oserrstr)
DBPROCESS *dbproc;
int severity;
int dberr;
int oserr;
char *dberrstr;
char *oserrstr;
{
	int err;
	char *errstr;

	if ((dbproc == NULL) || DBDEAD(dbproc))
		return INT_EXIT;
	else if (dberr != SYBESMSG) {
		err = oserr == DBNOERR ? dberr : oserr;
		errstr = oserr == DBNOERR ? dberrstr : oserrstr;
		printf("ErrHandler(): %d, %s\n", err, errstr);
	}
	return INT_CANCEL;
}
