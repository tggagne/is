static char *SCCSID = "@(#)isFPrintf.c	1.2 07/24/95";
#include "is.h"

int isFPrintf(char *control, ...)
{
	int retval = 0;
	va_list(parms);

	va_start(parms, control);

	if (!isData.suppress)
		retval = vfprintf(isData.out, control, parms);

	va_end(parms);
	return retval;
}
