static char *SCCSID = "%W% %G%";
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "is.h"

int isFile(dbproc, b, s, argc, argv)
DBPROCESS *dbproc;
int *b;
int *s;
int argc;
char **argv;
{
	FILE *fp, *oldfp;
	int action;
	struct isInNode *node;
	char orig[2048], input[2048];

	action = DONOTHING;

	if (argc == 2) {
		if (fp = fopen(argv[1], "r")) {
			if (strcmp(argv[0], ":r") == 0) {
				node = (struct isInNode *) isData.InQueue->tail;

				oldfp = node->in;
				node->in = fp;
				while (fgets(orig, 2048, fp) == orig) {
					isTokens(orig, input);
					if (input[0] == ':')
						action = isSpecial(isData.dbproc, &isData.batch, &isData.sequence, input);
					else {
						isBangInsert(dbproc, b, s, input);
						(*s)++;
					}
				}
				node->in = oldfp;
				fclose(fp);
			}
			else if (strcmp(argv[0], ":<") == 0) {
				node = (struct isInNode *) malloc(sizeof (struct isInNode));
				node->in = fp;
				InsertTail(isData.InQueue, (Node *) node);
			}
			else
				action = DOSYNTAX;
		}
		else {
			printf("error opening '%s' : %s\n", argv[1], strerror(errno));
			action = DOERROR;
		}
	}
	else
		action = DOSYNTAX;

	return action;
}
