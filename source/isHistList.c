static char *SCCSID = "@(#)isHistList.c	1.2 07/24/95";
#include <stdlib.h>
#include <string.h>
#include "is.h"

void isHistList(argc, argv)
int argc;
char **argv;
{
	int detail;
	int b, s, size;
	struct isHistoryNode *node;
	char *line, *sol, *eol;

	line = malloc(isData.columns);

	detail = (strcmp(argv[0], ":H") == 0);

	for (b = isData.batch - HISTORYNODES, node = (struct isHistoryNode *) isData.isHistory->head;
	     node != (struct isHistoryNode *) QNULL;
	     node = (struct isHistoryNode *) node->node.next, b++) {
		if (node->command) {
			strncpy(line, node->command, isData.columns);
			for (s = 1, sol = eol = line; *sol; s++) {
				if (eol = strchr(sol, '\n'))
					*eol = 0;
				else
					eol = sol + strlen(sol) - 1;
				isPrintf("%d.%d> %.*s\n", b, s, isData.columns,
					 sol);
				sol = eol + 1;
				if (!detail)
					break;
			}
		}
	}
}
