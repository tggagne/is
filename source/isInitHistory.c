static char *SCCSID = "@(#)isInitHistory.c	1.2 07/24/95";
#include <string.h>
#include "is.h"

int isInitHistory()
{
	if (isData.history)
		free(isData.history);
	isData.history = strdup("");
}
