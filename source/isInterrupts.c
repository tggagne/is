static char *SCCSID = "@(#)isInterrupts.c	1.4 08/21/96";
#include <termios.h>
#include <sys/ioctl.h>
#include <signal.h>
#include "is.h"

static void isHandler();
static int isChkIntr(), isHndlIntr();

void isInterrupts()
{
	signal(SIGALRM, isHandler);
	signal(SIGINT, isHandler);
	signal(SIGPIPE, isHandler);
	signal(SIGCONT, isHandler);
	/*
	 * signal(SIGWINCH, isHandler);
	 */
}

void isDBInterrupts(dbproc)
{
	dbsetinterrupt(dbproc, isChkIntr, isHndlIntr);
}

static void isHandler(int sig)
/*
int sig;
int code;
struct sigcontext *scp;
char *addr;
*/
{
	struct termios termios;

	isData.interrupted = sig;
	switch (sig) {
		case SIGALRM:
			if (isData.alarm_seconds) {
				ioctl(fileno(((struct isInNode *) isData.InQueue->tail)->in), TIOCSTI, "g");
				ioctl(fileno(((struct isInNode *) isData.InQueue->tail)->in), TIOCSTI, "o");
				ioctl(fileno(((struct isInNode *) isData.InQueue->tail)->in), TIOCSTI, "\n");
			}
			break;

		case SIGINT:
		case SIGCONT:
			isData.alarm_seconds = 0;
			if (isData.busy == 0)
				ioctl(fileno(((struct isInNode *) isData.InQueue->tail)->in), TIOCSTI, "\n");
			break;

		case SIGPIPE:
			fprintf(stderr, "from pipe : %s\n", strerror(pclose(isData.out) >> 8));
			isData.out = stdout;
			break;

		case SIGWINCH:
			break;

		default:
			fprintf(stderr, "signal(%d)\n", sig);
			break;
	}
	signal(sig, isHandler);
}

static int isChkIntr(dbproc)
DBPROCESS *dbproc;
{
	int retval = (isData.last_signal = isData.interrupted) != 0;
	isData.interrupted = 0;
	return retval;
}

static int isHndlIntr(dbproc)
DBPROCESS *dbproc;
{
	int retval = INT_CANCEL;

	if (isData.last_signal == SIGCONT) {
		isCmdbuf();
		retval = INT_CONTINUE;
	}
	return retval;
}
