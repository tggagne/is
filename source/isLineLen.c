static char *SCCSID = "@(#)isLineLen.c	1.2 07/24/95";
#include "is.h"

int isLineLen(DBPROCESS *dbproc, int width)
{
	int retval;
	char value[32];

	if (width == -1)
		isData.columns = width = isWinSize();

	sprintf(value, "%d", width);
	retval = dbsetopt(dbproc, DBPRLINELEN, value, -1) == SUCCEED ? DOOK : DOERROR;

	return retval;
}
