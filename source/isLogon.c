/*
the REQUEST_METHOD for this is POST, which hides the
command line from the browser.  When the method is
POST, what would normally be the QUERY_STRING is
provided in stdin, of CONTENT_LENGTH length
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv, char **envp)
{
	char *cp, *save, *request;

	cp = getenv("REQUEST_METHOD");
	if (strcmp(cp, "POST") == 0) {
		cp = getenv("CONTENT_LENGTH");
		save = request = malloc(atoi(cp) + 2);
		gets(request);

		while (cp = strtok(request, "&")) {
			request = 0;
			if (strstr(cp, "isUser="))
				printf("Set-Cookie: isUser=%s; path=/;\n", cp + 7);
			if (strstr(cp, "isPass="))
				printf("Set-Cookie: isPass=%s; path=/;\n", cp + 7);
			if (strstr(cp, "isHost="))
				printf("Set-Cookie: isHost=%s; path=/;\n", cp + 7);
			if (strstr(cp, "isDB="))
				printf("Set-Cookie: isDB=%s; path=/;\n", cp + 5);
			if (strstr(cp, "isBorder="))
				printf("Set-Cookie: isBorder=%s; path=/;\n", cp + 9);
			if (strstr(cp, "isPadding="))
				printf("Set-Cookie: isPadding=%s; path=/;\n", cp + 10);
		}
		free(request);
		puts("Content-type: text/html\n");
		puts("<HTML><BODY><H1>Thank You.</H1><P>");
		puts("You may have to hit the BACK button twice in your browser");
		puts("to return you to the menu.</P></BODY></HTML>");
	}

	return (0);
}
