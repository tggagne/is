#include <time.h>
#include <termios.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "is.h"

int isLoop()
{
	char *cp, *editor, *input, *orig;
	int len, didIbreak, action, dont_prompt = 0;
	struct isInNode *node;
	char pstring[128];
	char tokenized[INPUTSIZE];
	time_t startTime, endTime, elapsedTime;

	orig  = malloc(INPUTSIZE);
	input = isData.input;
	isData.batch = isData.sequence = 1;
	isInitHistory();
	editor = getenv("EDITOR");

	do {
		didIbreak = 0;
		if (dont_prompt == 0) {
			isTokens(isData.prompt, pstring);
			isPrintf("%s", pstring);
		}
		dont_prompt = 0;
		for (isData.sequence = 1;
		     fgets(orig, INPUTSIZE, (node = (struct isInNode *) isData.InQueue->tail)->in) == orig;
		     isData.sequence++, isTokens(isData.prompt, pstring), isPrintf("%s", pstring)) {
/*
if the input line ends with a '\', then
continue reading until it doesn't.  this allows
for multi-line invokations of subshells like nawk
*/
			for (len = strlen(orig); orig[len - 1] == '\\' || strcmp(&orig[len-2], "\\\n") == 0; len = strlen(orig)) {
				cp = strrchr(orig, '\\');
				if (fgets(cp, INPUTSIZE, (node = (struct isInNode *) isData.InQueue->tail)->in) != cp)
					break;
			}

			action = DONOTHING;

/*
token substitution now *always* happens here in isLoop
since macro processing is valuable for all command types,
not just SQL.
*/
			isTokens(orig, input);

			if ((isatty(fileno(node->in)) == 0) || isData.echo)
				isPrintf("%s", input);

			if ((isData.interrupted == SIGINT) || (strcmp("reset\n", input) == 0)) {
				isData.alarm_seconds = 0;
				isData.sequence = 0;
				isData.interrupted = 0;
				isInitHistory();
			}
			else if (isData.interrupted == SIGCONT) {
				isData.interrupted = 0;
				isCmdbuf();
			}
			else if (input[0] == '!')
				action = isBang(isData.dbproc, &isData.batch, &isData.sequence, input);	/* process history-type commands */

			else if (input[0] == ':')
				action = isSpecial(isData.dbproc, &isData.batch, &isData.sequence, input);	/* process special commands */

			else if (strncmp("go ", input, 3) == 0 || strncmp("go\n", input, 3) == 0) {
				action = DOEXECUTE;
				sscanf(input, "go%*s%hd%*s", &isData.alarm_seconds);
			}
			else if ((strcmp("quit\n", input) == 0) || (strcmp("exit\n", input) == 0)) {
				didIbreak = 1;
				break;
			}
			else if ((editor != (char *) NULL && strncmp(editor, input, strlen(editor)) == 0) || (strcmp("vi\n", input) == 0))
				isEdit(isData.dbproc, &isData.batch, &isData.sequence, input);

			else {
				if (isData.sequence == 1)
					isInitHistory();
				isCatHistory(input);
			}

			if (action == DOEXECUTE) {
				isAddHistory(isData.history);
				isData.interrupted = 0;	/* clear any outstanding ^Cs before begining query */
				startTime = time(NULL);
				isSqlExec();	/* execute SQL and process results */
				{
					char timeCommand[1024];
					elapsedTime = difftime(time(NULL), startTime);
					sprintf(timeCommand, ":define ELAPSEDSECONDS=%ld", elapsedTime);
					isDefine(timeCommand, NULL, NULL);
				}
				isData.sequence = 0;
				++isData.batch;
			}
		}
/*
	When interrupts occur, the for-loop breaks.  Before re-entering
	the for-loop, handle interrupts here.
*/
		if (isData.interrupted == SIGWINCH) {	/* window resized */
			isData.interrupted = 0;
			isLineLen(isData.dbproc, -1);
			dont_prompt = 1;
		}

		if (didIbreak || feof(node->in)) {
			fclose(node->in);
			free((char *) RemoveTail(isData.InQueue));
		}
	} while (isData.InQueue->tail != QNULL);
}
