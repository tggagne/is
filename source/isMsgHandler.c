#include <string.h>
#include "is.h"

int isMsgHandler(dbproc, msgno, msgstate, severity, msgtext, srvname, procname, line)
DBPROCESS *dbproc;
DBINT msgno;
int msgstate;
int severity;
char *msgtext;
char *srvname;
char *procname;
int line;
{
	int len;
	isMsgText *node;

	switch (msgno) {
		case 5701:	/* changed databases.. */
			isData.dbname = dbproc->dbcurdb;
		case 5703:
		case 5704:
			break;
		default:
			if (severity != 0) {
				printf("Msg %d, Level %d, State %d\n", msgno, severity, msgstate);
				printf("Server '%s', Procedure '%s', Line %d\n", srvname, procname, line);
				len = strlen(msgtext);
				if (msgtext[len - 1] != '\n')
					printf("%s\n", msgtext);
				else
					printf("%s", msgtext);
			}
			else {
				if (msgtext[0] == ':') {
					node = (isMsgText *) malloc(sizeof *node);
					node->text = strdup(msgtext);
					InsertTail(isData.isMsgQueue, (Node *) node);
				}
				else {
					if (isData.html)
						isFPrintf("%s", "<PRE>");

					len = strlen(msgtext);
					if (msgtext[len - 1] != '\n')
						isFPrintf("%s\n", msgtext);
					else
						isFPrintf("%s", msgtext);

					if (isData.html)
						isFPrintf("%s", "</PRE>");
				}
			}
			break;
	}
	return 0;
}
