static char *SccsId = "%W% %G%";
/* isOptions.c

Description:    Initializes all is's options and parses the command line.

Update History:

28nov94					Fixed processing of -P followed by another
						switch without an argument for -P.  Decreased
						optind by 1 and set password to all zeros.


Programming Note:

The processing for the -P argument is farily convoluted.  There are
three versions of appearance:
	1) is -Uusername -Ppassword
	2) is -Uusername -P
	3) is -Uusername -P -more options...
getopt() handles #1 OK.  It doesn't handle #2 and #3.  #2 causes
and error since a argument didn't follow.  This is handled by the
default: case below.  with #3, getopt() eats-up the next switch
causing things to get confused.  Below, we check to see if the
password is actually another switch.  If it is, we correct the
pointer to the next argument.

****************************************************************/
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include "is.h"

extern char *getfilename(char *);

int isOptions(int argc, char **argv)
{
	int c, fd, retval = 1;
	struct isInNode *node;
	char command[256], *cp, *http_cookie;
	extern char *optarg;
	extern int optind, opterr, optopt;

	opterr = 0;

	isData.InQueue = QueueAlloc(1);
	node = (struct isInNode *) malloc(sizeof (struct isInNode));
	node->in = stdin;
	InsertTail(isData.InQueue, (Node *) node);

	isData.dbVersion = DBVERSION_100;
	isData.columns = 80;
	isData.headers = 1;
	isData.rowcount = 1;
	isData.retstatus = 1;
	isData.out = stdout;
	isData.echo = 0;
	isData.csv = 0;
	isData.PassProvided = FALSE;

	strcpy(isData.format, "");
	strcpy(isData.AppName, getfilename(argv[0]));

	isData.spacing = 5;

	if (cp = getenv("DSQUERY"))
		strcpy(isData.Server, cp);

	while ((c = getopt(argc, argv, "-A:CD:HI:P:S:U:d:ei:mo:s:vw:x")) != -1) {
		switch (c) {
			case 1:
				/* ignore arguments without switches */
				break;
				
			case 'A':
				if (optarg)
					strcpy(isData.AppName, optarg);
				else
					retval = 0;
				break;

			case 'C':
				if (http_cookie = getenv("HTTP_COOKIE")) {
					isData.cookie = 1;
					while (cp = strtok(http_cookie, "; ")) {
						http_cookie = 0;
						putenv(strdup(cp));
					}
					if (cp = getenv("isBorder"))
						isData.border = atoi(cp);
					if (cp = getenv("isPadding"))
						isData.padding = atoi(cp);
					if (cp = getenv("isSpacing"))
						isData.spacing = atoi(cp);
				}
				break;

			case 'D':
				if (optarg) {
					sprintf(command, ":define %s", optarg);
					isDefine(command, 0, "");
				}
				else
					retval = 0;
				break;

			case 'H':
				if (isData.html)
					isData.html = 0;
				else {
					isData.html = 1;
					isData.headers = 0;
					isData.rowcount = 0;
					isData.retstatus = 0;
					isData.width = 100;
				}
				break;

			case 'U':
				if (optarg) {
					strcpy(isData.UserName, optarg);
					if (isData.cookie) {
						cp = getenv(optarg);
						if (cp)
							strcpy(isData.UserName, cp);
					}
				}

				else
					retval = 0;
				break;

			case 'P':
				isData.PassProvided = TRUE;
				if (optarg) {
					if (*optarg == '-')
						optind--;
					else
						strcpy(isData.Password, optarg);
					if (isData.cookie) {
						cp = getenv(optarg);
						if (cp)
							strcpy(isData.Password, cp);
					}
				}
				break;

			case 'S':
				if (optarg) {
					strcpy(isData.Server, optarg);
					if (isData.cookie) {
						cp = getenv(optarg);
						if (cp)
							strcpy(isData.Server, cp);
					}
				}
				break;

			case 'd':
				if (optarg) {
					strcpy(isData.InitDataBase, optarg);
					if (isData.cookie) {
						cp = getenv(optarg);
						if (cp)
							strcpy(isData.InitDataBase, cp);
					}
				}
				break;

			case 'e':
				isData.echo = 1;
				break;

			case 'I':
			case 'i':
				if (optarg) {
					if (c == 'I') {
						node = (struct isInNode *) malloc(sizeof (*node));
						InsertTail(isData.InQueue, (Node *) node);
					}
					else
						node = (struct isInNode *) isData.InQueue->head;

					if ((node->in = fopen(optarg, "r")) == NULL) {
						fprintf(stderr, "\n%s : %s\n", optarg, strerror(errno));
						retval = 0;
					}
				}
				else
					retval = 0;
				break;

			case 'm':
				isData.dbVersion = DBVERSION_46;
				break;

			case 'o':
				if (optarg) {
					if ((fd = open(optarg, O_WRONLY | O_CREAT | O_TRUNC, 0666)) != -1)
						dup2(fd, 1);
					else {
						fprintf(stderr, "\n%s : %s\n", optarg, strerror(errno));
						retval = 0;
					}
				}
				else
					retval = 0;
				break;

			case 's':
				if (optarg) {
					isData.serviceName = strdup(optarg);
				}
				break;

			case 'v':
				isData.vertical = 1;
				break;

			case 'w':
				if (optarg)
					isData.columns = atoi(optarg);
				break;

			case 'x':
				isData.xml = 1;
				break;

			default:
				/* special case for when -P has no argument */
				if (optopt == 'P')
					isData.PassProvided = TRUE;
				else
					retval = 0;
				break;
		}
	}

	if (isData.html) {
		// isFPrintf("Content-type: text/html\n\n");
		isFPrintf("<HTML><HEAD><TITLE>is Session Results</TITLE></HEAD>\n");
		isFPrintf("<BODY BGCOLOR=#ffffff>");
	}
	return retval;
}
