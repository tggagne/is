static char *SCCSID = "@(#)isOut.c	1.2 07/24/95";
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "is.h"

int isOut(dbproc, command, argc, argv)
DBPROCESS *dbproc;
char *command;
int argc;
char **argv;
{
	int retval;
	struct stat buf;
	char openmode[2];
	int doapipe;

	if (isData.out != stdout) {
		fstat(fileno(isData.out), &buf);
		if (S_ISFIFO(buf.st_mode))
			isPrintf("exit(%d)\n", pclose(isData.out));
		else
			fclose(isData.out);
		retval = DOOK;
	}
	isData.out = stdout;

	doapipe = strcmp(argv[0], ":|") == 0;

	if (doapipe || (argc == 2)) {
		if (doapipe)
			isData.out = isPOut(command);
		else {
			strcpy(openmode, "w");
			if (strcmp(argv[0], ":>>") == 0)
				strcpy(openmode, "a");
			isData.out = fopen(argv[1], openmode);
		}

		if (isData.out)
			retval = DOOK;
		else {
			isData.out = stdout;
			printf("%s : %s\n", argv[1], strerror(errno));
			retval = DOERROR;
		}
	} else if (argc > 2)
		retval = DOSYNTAX;

	return retval;
}
