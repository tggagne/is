static char *SccsId = "@(#)isPassword.c	1.3 08/11/95";
/* isPassword.c

Description:    handles the reading of the user's password

Update History:

02jul92                 Added module header for checkin to rel1_1
???????                 Original implementation
****************************************************************/
#include "termios.h"
#include "is.h"

int isPassword()
{
	int len;
	int fd;
	struct termios termios_p[2];

	if (isData.PassProvided == FALSE) {
		tcgetattr(0, &termios_p[0]);
		tcgetattr(0, &termios_p[1]);
		termios_p[1].c_lflag &= ~ECHO;
		tcsetattr(0, TCSANOW, &termios_p[1]);
		printf("Password: ");
		fgets(isData.Password, sizeof(isData.Password), stdin);
		puts("");
		len = strlen(isData.Password);
		isData.Password[len - 1] = 0;
		tcsetattr(0, TCSANOW, &termios_p[0]);
	}
}
