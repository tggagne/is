static char *SCCSID = "@(#)isPrintf.c	1.2 07/24/95";
#include <stdarg.h>
#include "is.h"

int isPrintf(char *control, ...)
{
	int fd, retval = 0;
	va_list(parms);

	va_start(parms, control);

	fd = fileno(((struct isInNode *) isData.InQueue->tail)->in);
	if (isatty(fd) || (isatty(fd) == 0 && isData.echo))
		retval = vprintf(control, parms);

	fflush(stdout);
	va_end(parms);
	return retval;
}
