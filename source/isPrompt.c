static char *SCCSID = "@(#)isPrompt.c	1.2 07/24/95";
#include <string.h>
#include "is.h"

int isPrompt(command)
char *command;
{
	char *cp;
	int retval = DOOK;

	if (cp = command + strcspn(command, "\"'")) {
		if (isData.prompt)
			free(isData.prompt);
		isData.prompt = strdup(cp + 1);
		if (cp = strrchr(isData.prompt, *cp))
			*cp = 0;
	} else
		retval = DOSYNTAX;
	return retval;
}
