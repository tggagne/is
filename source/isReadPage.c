static char *SCCSID = "@(#)isReadPage.c	1.2 07/24/95";
#include <stdlib.h>
#include <string.h>
#include "is.h"

int isReadPage(DBPROCESS *dbproc, int argc, char **argv)
{
	FILE *fp;
	int size, action;
	DBINT pageno;
	char buf[2048], name[DBMAXNAME];

#ifdef HAVE_DBREADPAGE
	action = DONOTHING;

	if (argc == 3) {
		strcpy(name, argv[1]);
		pageno = atoi(argv[2]);
		if (strcmp(".", name) == 0)
			strcpy(name, dbname(dbproc));
		if ((size = dbreadpage(dbproc, name, pageno, buf)) != -1) {
			zap_display(buf, 2048, 16, pageno * 2048, isFPrintf);
			fflush(isData.out);
		} else
			action = DOERROR;
		dbfreebuf(dbproc);
		dbcancel(dbproc);
	}
	 /* if (sscanf .. */ 
	else
		action = DOSYNTAX;
#endif
	return action;
}
