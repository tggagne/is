static char *SCCSID = "@(#)isResults.c	1.1 08/09/96";
#include <alloca.h>
#include <malloc.h>
#include <time.h>
#include "is.h"

static void rtrim(char *);
extern char *bprintf(char *, char *, char *);

#define DBPROC isData.dbproc

int isResults()
{
	char *buffer, *op, *cp;
	int buf_len, rc, column, numalts;
	STATUS rowstat;
	DBINT rowcount;

	time_t today;

	isData.busy = 1;

	buffer = (char *) 0;
	if (DBCMDROW(DBPROC))
		if (buffer = malloc(128 + (buf_len = dbspr1rowlen(DBPROC))))
			if (isData.headers) {
				dbsprhead(DBPROC, buffer, buf_len);
				rtrim(buffer);
				if (buffer[0] == '\n')
					memmove(buffer, buffer + 1, strlen(buffer));
				isFPrintf("%s\n", buffer);
				dbsprline(DBPROC, buffer, buf_len, '-');
				rtrim(buffer);
				if (buffer[0] == '\n')
					memmove(buffer, buffer + 1, strlen(buffer));
				isFPrintf("%s\n", buffer);
			}
	if (DBROWS(DBPROC)) {
		while ((rowstat = dbnextrow(DBPROC)) != NO_MORE_ROWS) {
			if (rowstat != REG_ROW) {
				numalts = dbnumalts(DBPROC, rowstat);
				for (column = 1; column <= numalts; column++) {
					op = dbprtype(rc = dbaltop(DBPROC, rowstat, column));
					rc = dbconvert(DBPROC,
							  dbalttype(DBPROC, rowstat, column),
							  dbadata(DBPROC, rowstat, column),
							  dbadlen(DBPROC, rowstat, column),
							  SYBCHAR,
							  buffer,
							  -1);
				}
			}
			dbspr1row(DBPROC, buffer, buf_len);
			rtrim(buffer);
			if (isData.format[0]) {
				cp = (char *) alloca(4192);
				bprintf(cp, isData.format, buffer);
				if (cp[0] == '\n')
					memmove(cp, cp + 1, strlen(cp));
				isFPrintf("%s\n", cp);
			}
			else {
				if (buffer[0] == '\n')
					memmove(buffer, buffer + 1, strlen(buffer));
				isFPrintf("%s\n", buffer);
			}
		}
	}
	if (buffer)
		free(buffer);
	if (isData.rowcount) {
		if ((rowcount = DBCOUNT(DBPROC)) != -1) {
			if (rowcount == 1)
				printf("(1 row affected)\n");
			else
				printf("(%d rows affected)\n", rowcount);
		}
	}
	if (dbhasretstat(DBPROC) == TRUE && isData.retstatus)
		printf("(return status = %d)\n", dbretstatus(DBPROC));
}

static void rtrim(string)
char *string;
{
	char *cp;

	cp = strchr(string, 0) - 1;

	while (cp >= (char *) string && (isspace(*cp)))
		*cp-- = 0;
}
