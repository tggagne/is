#include <alloca.h>
#include <malloc.h>
#include <time.h>
#include <string.h>
#include "is.h"

#define DBPROC isData.dbproc

char *isResultsConvert(int column, int rowstat)
{
	static char buffer[2048];
	int srctype, desttype;
	DBDATEREC dbdaterec;
	DBFLT8 dval;
	struct tm tm;
	char *cp;

	if (rowstat > 0) /* ALT_ROW */
		srctype = dbalttype(DBPROC, rowstat, column);
	else
		srctype = dbcoltype(DBPROC, column);

	switch (srctype) {
		case SYBDATETIME:
		case SYBDATETIME4:
			desttype = SYBDATETIME;
			break;

		case SYBFLT8:
		case SYBMONEY:
		case SYBMONEY4:
			desttype = SYBFLT8;
			break;

		default:
			desttype = SYBCHAR;
			break;
	}

	if (rowstat > 0) /* ALT_ROW */
		dbconvert(DBPROC,
			  dbalttype(DBPROC, rowstat, column),
			  dbadata(DBPROC, rowstat, column),
			  dbadlen(DBPROC, rowstat, column),
			  desttype,
			  buffer,
			  -1);
	else
		dbconvert(DBPROC,
			  dbcoltype(DBPROC, column),
			  dbdata(DBPROC, column),
			  dbdatlen(DBPROC, column),
			  desttype,
			  buffer,
			  -1);
/*
We need to do something with text fields that
have embedded newlines.  
*/
	if (desttype == SYBCHAR) {
		while (cp = strstr(buffer, "\x0d\x0a")) {
			/* memmove(cp + 1, cp, strlen(cp)); */
			memcpy(cp, "\\n", 2);
		}
	}

	else if (desttype == SYBDATETIME) {
		dbdatecrack(DBPROC, &dbdaterec, (DBDATETIME *) buffer);
		tm.tm_sec = dbdaterec.datesecond;
		tm.tm_min = dbdaterec.dateminute;
		tm.tm_hour = dbdaterec.datehour;
		tm.tm_mday = dbdaterec.datedmonth;
		tm.tm_mon = dbdaterec.datemonth;
		tm.tm_year = dbdaterec.dateyear - 1900;
		tm.tm_wday = dbdaterec.datedweek;
		tm.tm_yday = dbdaterec.datedyear;
		strftime(buffer, 2048, "%b %e %Y %I:%M""%p", &tm);
	}
	else if (desttype == SYBFLT8) {
		dval = (double) *(double *) buffer;
		if (!isData.csv && (srctype == SYBMONEY || srctype == SYBMONEY4)) {
			sprintf(buffer, "%s", currency(dval));

			if (cp = strchr(buffer, '.'))
				*cp = 0;
		}
		else
			sprintf(buffer, "%.4f", dval);
	}

	return buffer;
}
