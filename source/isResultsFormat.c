#include <stdlib.h>
#include <alloca.h>
#include "is.h"

char *isResultsFormat(char *buffer, char *format)
{
	int len;
	char *save;

	len = strlen(buffer);

	save = alloca(len + 1);
	strcpy(save, buffer);

	sprintf(buffer, format, save, save, save, save, save, save, save, save, save);

	return buffer;
}
