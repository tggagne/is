/* %W% %G% */
#include <alloca.h>
#include <malloc.h>
#include <time.h>
#include <string.h>
#include "is.h"

extern char *bprintf(char *, char *, char *);

#define DBPROC isData.dbproc

int isResultsHTML()
{
	tabEntry *te;
	static char buffer[2048];
	char *cp, *op, *align, *colfmts[256];
	int desttype, buf_len, rc, altcol, column, numalts, numcols, colid;
	STATUS rowstat;
	DBINT rowcount;
	DBDATEREC dbdaterec;
	DBFLT8 dval;
	struct tm tm;

	isData.busy = 1;

	if (!isData.vertical)
		isFPrintf("<TABLE CELLSPACING=%d BORDER=%d CELLPADDING=%d WIDTH=%d%% NOWRAP=NOWRAP>\n", isData.spacing, isData.border, isData.padding, isData.width);

	if (DBCMDROW(DBPROC)) {
		numcols = dbnumcols(DBPROC);
		for (column = 1; column <= numcols; column++) {
			colfmts[column] = NULL;
			if ((te = tabLookup(isData.ColFmts, dbcolname(DBPROC, column))) != QNULL)
				colfmts[column] = te->def;

			switch (dbcoltype(DBPROC, column)) {
				case SYBCHAR:
				case SYBTEXT:
				case SYBDATETIME:
				case SYBDATETIME4:
					align = "LEFT";
					break;
				default:
					align = "RIGHT";
					break;
			}
			if (!isData.vertical)
				isFPrintf("<TH VALIGN=\"BOTTOM\">%s</TH>", dbcolname(DBPROC, column));
		}
		if (!isData.vertical)
			isFPrintf("\n");
	}
	if (DBROWS(DBPROC)) {
		while ((rowstat = dbnextrow(DBPROC)) != NO_MORE_ROWS) {
			if (!isData.vertical)
				isFPrintf("<TR>");

			if (rowstat != REG_ROW) {
				align = "RIGHT";
				numalts = dbnumalts(DBPROC, rowstat);
				for (column = 1; column <= numcols; column++) {
					buffer[0] = 0;
					for (altcol = 1; altcol <= numalts; altcol++) {
						colid = dbaltcolid(DBPROC, rowstat, altcol);
						if (colid == column)
							strcpy(buffer, isResultsConvert(altcol, rowstat));
					}
					if (!isData.vertical)
						isFPrintf("<TD COLSTART=\"%d\" VALIGN=\"TOP\" ALIGN=\"%s\"><B>%s</B></TD>", column, align, buffer);
				}
			}
			else {
				for (column = 1; column <= numcols; column++) {
					desttype = SYBCHAR;
					switch (dbcoltype(DBPROC, column)) {
						case SYBDATETIME:
						case SYBDATETIME4:
						case SYBCHAR:
						case SYBTEXT:
						case SYBIMAGE:
							align = "LEFT";
							break;

						default:
							align = "RIGHT";
							break;
					}

					cp = isResultsConvert(column, rowstat);
					if (colfmts[column])
						cp = isResultsFormat(cp, colfmts[column]);

					if (isData.vertical) {
						char *command;
						command = alloca(strlen(cp) + strlen(dbcolname(DBPROC, column)) + 3);
						sprintf(command, "%s=%s\n", dbcolname(DBPROC, column), cp);
						isFPrintf(command);
						if (strncmp(":define ", command, 8) == 0)
							isDefine(command, NULL, 0);
					}
					else
						isFPrintf("<TD COLSTART=\"%d\" VALIGN=\"TOP\" ALIGN=\"%s\">%s</TD>",
							  column, align, cp);
				}
			}
			if (!isData.vertical)
				isFPrintf("</TR>\n");
		}
	}
	if (!isData.vertical)
		isFPrintf("\n</TABLE>");
	if (isData.rowcount) {
		if ((rowcount = DBCOUNT(DBPROC)) != -1) {
			if (rowcount == 1)
				printf("(1 row affected)\n");
			else
				printf("(%d rows affected)\n", rowcount);
		}
	}
	if (dbhasretstat(DBPROC) == TRUE && isData.retstatus)
		printf("(return status = %d)\n", dbretstatus(DBPROC));
}

static rtrim(string)
char *string;
{
	int len;

	len = strlen(string);

	while (len != 0 && string[len - 1] == ' ')
		string[len--] = 0;
}
