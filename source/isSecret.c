static char *SCCSID = "@(#)isSecret.c	1.2 07/24/95";
#include "is.h"

void isSecret()
{
	int i;
	char *cp;

	for (i = 0; i < isData.argc; i++)
		if (memcmp("-P", cp = isData.argv[i], 2) == 0) {
			memset(cp, ' ', strlen(cp));
			if (strlen(cp) == 2 && (cp = isData.argv[i + 1]))
				memset(cp, ' ', strlen(cp));
		}
}
