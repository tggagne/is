static char *SCCSID = "%W% %G%";
#include <string.h>
#include "is.h"

int isSet(dbproc, argc, argv)
DBPROCESS *dbproc;
int argc;
char **argv;
{
	int retval;
	if (argc == 3) {
		if (strcmp(argv[1], "headers") == 0) {
			isData.headers = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "retstatus") == 0) {
			isData.retstatus = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "rowcount") == 0) {
			isData.rowcount = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "html") == 0) {
			isData.html = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "xml") == 0) {
			isData.xml = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "border") == 0) {
			isData.border = atoi(argv[2]);
			retval = DOOK;
		} else if (strcmp(argv[1], "padding") == 0) {
			isData.padding = atoi(argv[2]);
			retval = DOOK;
		} else if (strcmp(argv[1], "spacing") == 0) {
			isData.spacing = atoi(argv[2]);
			retval = DOOK;
		} else if (strcmp(argv[1], "vertical") == 0) {
			isData.vertical = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "suppress") == 0) {
			isData.suppress = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "wholemoney") == 0) {
			isData.wholemoney = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else if (strcmp(argv[1], "width") == 0) {
			isData.width = atoi(argv[2]);
			retval = DOOK;
		} else if (strcmp(argv[1], "csv") == 0) {
			isData.csv = strcmp(argv[2], "on") == 0;
			retval = DOOK;
		} else
			retval = DOOPTION;
	} else
		retval = DOSYNTAX;

	return retval;
}
