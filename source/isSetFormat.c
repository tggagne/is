static char *SCCSID = "@(#)isSetFormat.c	1.2 07/24/95";
#include <string.h>
#include "is.h"

#define COLLAPSE memmove(cp + 1, cp + 2, strlen(cp + 1));

int isSetFormat(argc, command)
int argc;
char *command;
{
	int action = DOOK;
	char *cp;

	if (argc == 1)
		strcpy(isData.format, "");
	else {
		strcpy(isData.format, command + strlen(":printf "));
		while (cp = strstr(isData.format, "\\t")) {
			*cp = '\t';
			COLLAPSE
		}
		while (cp = strstr(isData.format, "\\n")) {
			*cp = '\n';
			COLLAPSE
		}
		while (cp = strstr(isData.format, "\\r")) {
			*cp = '\r';
			COLLAPSE
		}
		isData.format[strlen(isData.format) - 1] = NULL;
	}
	return action;
}
