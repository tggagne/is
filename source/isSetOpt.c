static char *SCCSID = "@(#)isSetOpt.c	1.2 07/24/95";
#include <string.h>
#include "is.h"

int isSetOpt(DBPROCESS *dbproc, int argc, char **argv)
{
	int retval;
	char value[32];

	if (argc >= 2) {
		if (strcmp(argv[1], "DBPRLINELEN") == 0)
			retval = isLineLen(dbproc, argc == 2 ? -1: atoi(argv[2]));
		else
			retval = DOOPTION;
	} else
		retval = DOSYNTAX;

	return retval;
}
