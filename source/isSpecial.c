static char *SCCSID = "@(#)isSpecial.c	1.6 09/16/96";
#include <string.h>
#include "is.h"

#define ISMAXWORDS 10

int isSpecial(DBPROCESS * dbproc, int *b, int *s, char *command)
{
	int argc;
	char *argv[ISMAXWORDS + 1];
	char arg[ISMAXWORDS][1024];
	int action = DONOTHING;

/*
before parsing the command into separate arguments, first check to
see if the command is one of the kind that wouldn't handle that
well.
*/
	if (memcmp(command, ":define", 7) == 0)
		action = isDefine(command, argc, argv);

	else {

		for (argc = 0; argc < ISMAXWORDS; argc++)
			argv[argc] = arg[argc];

		argc =
			sscanf(command, "%s%s%s%s%s%s%s%s%s%s", arg[0], arg[1], arg[2],
				   arg[3], arg[4], arg[5], arg[6], arg[7], arg[8], arg[9]);
		argv[argc] = 0;

		if (strcmp(":set", arg[0]) == 0)
			action = isSet(dbproc, argc, argv);

		else if (strcmp(":setopt", arg[0]) == 0)
			action = isSetOpt(dbproc, argc, argv);

		else if ((strcmp(":>", arg[0]) == 0)
				 || (strcmp(":>>", arg[0]) == 0)
				 || strcmp(":|", arg[0]) == 0)
			action = isOut(dbproc, command, argc, argv);

		else if ((strcmp(":r", arg[0]) == 0)
				 || (strcmp(":<", arg[0]) == 0))
			action = isFile(dbproc, b, s, argc, argv);

		else if ((strcmp(":h", arg[0]) == 0)
				 || (strcmp(":H", arg[0]) == 0))
			isHistList(argc, argv);

		else if (strcmp(":colfmt", arg[0]) == 0)
			action = isColFmt(command);

		else if (strcmp(":cgi-add", arg[0]) == 0)
			action = isCgiAdd(dbproc, b, s, command);

		else if (strcmp(":echo", arg[0]) == 0)
			action = isEcho(command + 6);

		else if (strcmp(":printf", arg[0]) == 0)
			action = isSetFormat(argc, command);

		else if (strcmp(":rp", arg[0]) == 0)
#ifdef HAVE_DBPREADPAGE
			action = isReadPage(dbproc, argc, argv);
#else
			puts("dbreadpage() not implemented in your libsybdb");
#endif

		else if (memcmp(":!", arg[0], 2) == 0)
			action = isSystem(command + 2);

		else if (strcmp(":?", arg[0]) == 0)
			action = isHelp();

		else if (strcmp(":prompt", arg[0]) == 0)
			action = isPrompt(command);

		else if (strcmp(":connect", arg[0]) == 0)
			action = isConnect(dbproc, argc, argv);

		else
			printf("I don't know '%s' yet.\n", command);

	}

	(*s)--;

	if (action == DOOK);		/*puts("Ok"); */
	else if (action == DOSYNTAX)
		puts("illegal syntax");
	else if (action == DOOPTION)
		puts("illegal option");
	else if (action == DOERROR)
		puts("FAIL");
	else;

	return action;
}
