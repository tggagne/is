static char *SCCSID = "@(#)isSqlExec.c	1.5 09/06/96";
#include <alloca.h>
#include <malloc.h>
#include <time.h>
#include <string.h>
#include "is.h"

#define DBPROC isData.dbproc

int isSqlExec()
{
	Node *node;
	isMsgText *text;

	isData.busy = 1;

	dbcmd(DBPROC, isData.history);

	if (dbsqlexec(DBPROC) == SUCCEED) {
		while (dbresults(DBPROC) != NO_MORE_RESULTS) {
			for (; (node = RemoveHead(isData.isMsgQueue)) != QNULL;) {
				text = (isMsgText *) node;
				isSpecial(DBPROC, &isData.batch, &isData.sequence, text->text);
				free(text->text);
			}
			if (isData.html)
				isResultsHTML();
			else if (isData.xml)
				isResultsXML();
			else if (isData.csv)
				isResultsCSV();
			else
				isResults();
		}
		fflush(isData.out);
	}
	if (isData.alarm_seconds)
		alarm(isData.alarm_seconds);
	isData.busy = 0;
}
