/*
	08/30/2005	tgagne
				terminate 'is' if a subprocess returns an error
*/

#include <stdlib.h>
#include <alloca.h>
#include "is.h"

int isSystem(char *command)
{
	int action, rc, retVal;

	action = DONOTHING;

	rc = system(command);
	retVal = rc >> 8;
	fprintf(stderr, "exit(%d)\n", retVal);

	if (retVal != 0)
		exit(retVal);

	return action;
}
