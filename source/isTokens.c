static char *SccsId = "@(#)isTokens.c	1.3 08/11/95";
/* isTokens.c

Description:    Substitutes TOKENS in the command buffer with
                                :defines from the deflist - does the same for
                                the promptstring.

Update History:

02jul92                 Added module header for checkin to rel1_1
???????                 Original implementation
****************************************************************/
#include <stdlib.h>
#include <string.h>
#include <alloca.h>
#include "is.h"

void isTokens(char *source, char *dest)
{
	char *token, *dupstr, *s1, *s;
	struct node *node;
	tabEntry *isnode;
	int insideParen, i;
	int didSomething = 0;

	s = dupstr = (char *) alloca(strlen(source) + 1);
	strcpy(dupstr, source);

	for (i = 0; source[i];) {
		s1 = dupstr + i;
		if (*s1 == '$') {
			if (memcmp(s1, "$DB", 3) == 0) {
				strcpy(dest, isData.dbproc->dbcurdb);
				dest += strlen(dest);
				i += 3;
				didSomething++;
			}
			else if (memcmp(s1, "$BATCH", 6) == 0) {
				sprintf(dest, "%d", isData.batch);
				dest += strlen(dest);
				i += 6;
				didSomething++;
			}
			else if (memcmp(s1, "$LINE", 5) == 0) {
				sprintf(dest, "%d", isData.sequence);
				dest += strlen(dest);
				i += 5;
				didSomething++;
			}
			else if (memcmp(s1, "$SERVER", 5) == 0) {
				sprintf(dest, "%s", isData.Server);
				dest += strlen(dest);
				i += 7;
				didSomething++;
			}
			else {
/*
if the symbol is surrounded with parens, we
want to gobble up the parens as well.
*/
				insideParen = 0;
				if (*(s1 + 1) == '(') {
					insideParen = 1;
					s1++;
				}

				token = strtok(s1 + 1, " .,-\n!@#$%^&*()_+|~-=\\`{}[]:\";'<>?/");
				if (token) {
					isnode = tabLookup(isData.isDefineSpace, token);
					if (isnode != QNULL) {
						strcpy(dest, isnode->def);
						dest += strlen(dest);
						i += strlen(token) + 1 + (insideParen * 2) ;
						didSomething++;
					}
					else
						i++, *dest++ = *s1;
				}
				else
					i++, *dest++ = *s1;
			}
		}
		else
			i++, *dest++ = *s1;
		dupstr[i] = source[i];	/* there may be a null in dupstr after strtok */
	}
	*dest = 0;
}
