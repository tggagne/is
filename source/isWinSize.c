static char *SCCSID = "@(#)isWinSize.c	1.2 07/24/95";
#include <stdio.h>
#include <termio.h>
#include <sys/termios.h>
#include "is.h"

int isWinSize(void)
{
	int cols = isData.columns, err, filedes;
	struct winsize winsize;

	if (isatty(filedes = fileno(stdout)))
		if ((err = ioctl(filedes, TIOCGWINSZ, &winsize)) == 0)
			cols = winsize.ws_col;

	return cols;
}
