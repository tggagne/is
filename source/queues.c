static char *SCCSID = "@(#)queues.c	1.2 07/24/95";
#include <stdlib.h>
#include "queues.h"

Node *InsertHead(Queue *q, Node *n)
{
	if (q->head == QNULL) {
		q->tail = q->head = n;
		n->previous = n->next = QNULL;
	}
	else {
		q->head->previous = n;
		n->next = q->head;
		n->previous = QNULL;
		q->head = n;
	}
	n->queue = q;
	q->count++;
	return n;
}

Node *InsertTail(Queue *q, Node *n)
{
	if (q->tail == QNULL) {
		q->tail = q->head = n;
		n->previous = n->next = QNULL;
	}
	else {
		q->tail->next = n;
		n->previous = q->tail;
		n->next = QNULL;
		q->tail = n;
	}
	n->queue = q;
	q->count++;
	return n;
}

Queue *QueueInit(q)
Queue *q;
{
	q->head = q->tail = QNULL;
	q->count = 0L;
	return q;
}

Queue *QueueAlloc(num)
int num;
{
	int i;
	Queue *q;

	q = (Queue *) calloc(sizeof(*q), num);

	for (i = 0; i < num; i++)
		QueueInit(&q[i]);

	return q;
}

Node *RemoveHead(Queue *q)
{
	Node *n;

	if ((n = q->head) != QNULL) {
		if (n->next != QNULL) {
			q->head = n->next;
			q->head->previous = QNULL;
		}
		else
			q->head = q->tail = QNULL;

		n->next = n->previous = QNULL;
		n->queue = QNULL;
		q->count--;
	}

	return n;
}

Node *RemoveTail(Queue *q)
{
	Node *n;

	if ((n = q->tail) != QNULL) {
		if (n->previous != QNULL) {
			q->tail = n->previous;
			n->previous->next = QNULL;
		}
		else
			q->head = q->tail = QNULL;

		n->next = n->previous = QNULL;
		n->queue = QNULL;
		q->count--;
	}

	return n;
}


Node *InsertAfter(Node *old, Node *new)
{
	if (old->next == QNULL)
		InsertTail(old->queue, new);
	else {
		new->queue = old->queue;
		new->next = old->next;
		new->previous = old;

		old->next->previous = new;
		old->next = new;
	}
	return new;
}

Node *InsertBefore(Node *old, Node *new)
{
	if (old->previous == QNULL)
		InsertHead(old->queue, new);
	else {
		new->queue = old->queue;
		new->next = old;
		new->previous = old->previous;

		old->previous->next = new;
		old->previous = new;
	}
	return new;
}

Node *RemoveNode(node)
Node *node;
{
	if (node->previous == QNULL)
		node = RemoveHead(node->queue);
	else if (node->next == QNULL)
		node = RemoveTail(node->queue);
	else {
		node->previous->next = node->next;
		node->next->previous = node->previous;
		node->next = node->previous = QNULL;
	}
	return node;
}

Node *QueueByteCmp(queue, type, offset, bstring, bstringlen, first)
Queue *queue;
int type;
int offset;
char *bstring;
int bstringlen;
Node *first;
{
	int result, found;
	Node *node;

	if (first == QNULL)
		node = queue->head;
	else
		node = first->next;

	for (found = 0; node != QNULL; node = node->next) {
		result = memcmp((char *) node + offset, bstring, bstringlen);
		switch (type) {
			case QEQ:
				if (result == 0)
					found = 1;
				break;
			case QGT:
				if (result > 0)
					found = 1;
				break;
			case QGE:
				if (result > 0 || result == 0)
					found = 1;
				break;
			case QLT:
				if (result < 0)
					found = 1;
				break;
			case QLE:
				if (result < 0 || result == 0)
					found = 1;
				break;
			case QNE:
				if (result != 0)
					found = 1;
				break;
			default:
				node = QNULL;	/* I don't recognize the operation - return QNULL */
				break;
		}
		if (found)
			break;
	}
	return node;
}

Queue *QueueFree(queue)
Queue *queue;
{
	free((char *) NodeFree(queue));
	return queue;
}

int QueueItems(Queue *q)
{
	int i;
	Node *node;

	for (i = 0, node = q->head; node != QNULL; i++, node = node->next);

	return i;
}

Node *QueueSearch(queue, type, offset, value, first)
Queue *queue;
int type;
int offset;
int value;
Node *first;
{
	int testval, found;
	Node *node;

	if (first == QNULL)
		node = queue->head;
	else
		node = first->next;

	for (found = 0; node != QNULL; node = node->next) {
		memcpy((char *) &testval, ((char *) node) + offset, sizeof (testval));
		switch (type) {
			case QEQ:
				found = testval == value;
				break;
			case QGT:
				found = testval > value;
				break;
			case QLT:
				found = testval < value;
				break;
			case QGE:
				found = testval >= value;
				break;
			case QLE:
				found = testval <= value;
				break;
			case QNE:
				found = testval != value;
				break;
			case QAND:
				found = testval & value;
				break;
			case QNAND:
				found = !(testval & value);
				break;
			case QOR:
				found = testval | value;
				break;
			case QXOR:
				found = testval ^ value;
				break;
			default:
				node = QNULL;	/* I don't recognize the operation - return QNULL */
				break;
		}
		if (found)
			break;
	}
	return node;
}

Queue *NodeFree(queue)
Queue *queue;
{
	Node *node;

	while ((node = RemoveTail(queue)) != QNULL)
		free(node);

	return queue;
}
