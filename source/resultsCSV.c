/* %W% %G% */
#include <alloca.h>
#include <malloc.h>
#include <time.h>
#include <string.h>
#include "is.h"

extern char *bprintf(char *, char *, char *);

#define DBPROC isData.dbproc

int isResultsCSV()
{
	tabEntry *te;
	static char buffer[2048];
	char *cp, *op, *align, *colfmts[256];
	int desttype, buf_len, rc, altcol, column, numalts, numcols, colid;
	STATUS rowstat;
	DBINT rowcount;

	isData.busy = 1;

	if (DBCMDROW(DBPROC)) {
		numcols = dbnumcols(DBPROC);
		for (column = 1; column <= numcols; column++) {
			colfmts[column] = NULL;
			if ((te = tabLookup(isData.ColFmts, dbcolname(DBPROC, column))) != QNULL)
				colfmts[column] = te->def;

			isFPrintf("%s,", dbcolname(DBPROC, column));
		}
		isFPrintf("\n");
	}

	if (DBROWS(DBPROC)) {
		while ((rowstat = dbnextrow(DBPROC)) != NO_MORE_ROWS) {
			if (rowstat != REG_ROW) {
				align = "RIGHT";
				numalts = dbnumalts(DBPROC, rowstat);
				for (column = 1; column <= numcols; column++) {
					buffer[0] = 0;
					for (altcol = 1; altcol <= numalts; altcol++) {
						colid = dbaltcolid(DBPROC, rowstat, altcol);
						if (colid == column)
							strcpy(buffer, isResultsConvert(altcol, rowstat));
					}
					isFPrintf("%s,", buffer);
				}
			}
			else {
				for (column = 1; column <= numcols; column++) {
					desttype = SYBCHAR;
					switch (dbcoltype(DBPROC, column)) {
						case SYBDATETIME:
						case SYBDATETIME4:
						case SYBCHAR:
						case SYBTEXT:
						case SYBIMAGE:
							align = "LEFT";
							break;

						default:
							align = "RIGHT";
							break;
					}

// if the column's value is null leave it empty, else use isResultsConvert()

					if (dbdatlen(DBPROC, column) == 0)
						cp = "";
					else
						cp = isResultsConvert(column, rowstat);

					if (colfmts[column])
						cp = isResultsFormat(cp, colfmts[column]);

					if (strcmp(align, "LEFT") == 0)
						isFPrintf("\"%s\",", cp);
					else
						isFPrintf("%s,", cp);
				}
			}
			isFPrintf("\n");
		}
	}
	if (isData.rowcount) {
		if ((rowcount = DBCOUNT(DBPROC)) != -1) {
			if (rowcount == 1)
				printf("(1 row affected)\n");
			else
				printf("(%d rows affected)\n", rowcount);
		}
	}
	if (dbhasretstat(DBPROC) == TRUE && isData.retstatus)
		printf("(return status = %d)\n", dbretstatus(DBPROC));
}

static rtrim(string)
char *string;
{
	int len;

	len = strlen(string);

	while (len != 0 && string[len - 1] == ' ')
		string[len--] = 0;
}
