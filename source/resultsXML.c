/*
this was copied from isResultsHTML as a starter
*/

#include <alloca.h>
#include <malloc.h>
#include <time.h>
#include "is.h"

extern char *bprintf(char *, char *, char *);

#define DBPROC isData.dbproc

int isResultsXML()
{
	tabEntry *te;
	static char buffer[2048];
	char *cp, *op, *colfmts[256];
	int buf_len, rc, altcol, column, numalts, numcols, colid;
	STATUS rowstat;
	DBINT rowcount;
	DBDATEREC dbdaterec;
	DBFLT8 dval;
	struct tm tm;

	isData.busy = 1;

	isFPrintf("<isResult>\n");
/*
collect column formats if available, for each column name

also counts the number of columns in the result set (numcols)
*/
	if (DBCMDROW(DBPROC)) {
		numcols = dbnumcols(DBPROC);
		for (column = 1; column <= numcols; column++) {
			colfmts[column] = NULL;
			if ((te = tabLookup(isData.ColFmts, dbcolname(DBPROC, column))) != QNULL)
				colfmts[column] = te->def;
		}
	}

	if (DBROWS(DBPROC)) {
		while ((rowstat = dbnextrow(DBPROC)) != NO_MORE_ROWS) {
			isFPrintf("\t<isRow>\n");

			if (rowstat != REG_ROW) {
				numalts = dbnumalts(DBPROC, rowstat);
				for (column = 1; column <= numcols; column++) {
					buffer[0] = 0;
					for (altcol = 1; altcol <= numalts; altcol++) {
						colid = dbaltcolid(DBPROC, rowstat, altcol);
						if (colid == column)
							strcpy(buffer, isResultsConvert(altcol, rowstat));
					}
				}
			}
			else {
				for (column = 1; column <= numcols; column++) {
					cp = isResultsConvert(column, rowstat);
					if (colfmts[column])
						cp = isResultsFormat(cp, colfmts[column]);

					isFPrintf("\t\t<isColumn name=\"%s\">%s</isColumn>\n", dbcolname(DBPROC, column), cp);
				}
			}
			isFPrintf("\t</isRow>\n");
		}
	}

	isFPrintf("\t<isResultInfo rows=\"%d\" status=\"%d\"/>\n", DBCOUNT(DBPROC), dbretstatus(DBPROC));

	isFPrintf("</isResult>\n");
}

static rtrim(string)
char *string;
{
	int len;

	len = strlen(string);

	while (len != 0 && string[len - 1] == ' ')
		string[len--] = 0;
}
