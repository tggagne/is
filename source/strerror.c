static char *SCCSID = "@(#)strerror.c	1.2 07/24/95";
extern int sys_nerr;
extern char *sys_errlist[];

char *strerror(error)
int error;
{
	return (error > 0 && error < sys_nerr) ? sys_errlist[error] : "";
}
