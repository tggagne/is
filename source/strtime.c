static char *SCCSID = "@(#)strtime.c	1.2 07/24/95";
#include <stdio.h>
#include <time.h>

char *strtime()
{
        time_t tval;

        tval = time((time_t *) 0);

        return ctime(&tval);
}
