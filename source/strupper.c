static char *SCCSID = "@(#)strupper.c	1.2 07/24/95";
#include <ctype.h>

char *strupper(string)
char *string;
{
        char *cp;

        for (cp = string; *cp; cp++)
             if (islower (*cp)) *cp = toupper(*cp);

        return string;
}
