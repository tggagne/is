static char *SCCSID = "@(#)tabfuncs.c	1.2 07/24/95";
#include <memory.h>
#include <string.h>
#include "tabfuncs.h"

tabSpace *tabAlloc(int hashsize)
{
	int i;
	tabSpace *ts;

	ts = (tabSpace *) calloc(hashsize, sizeof (*ts));

	ts[0].hashsize = hashsize;

	for (i = 0; i < hashsize; i++)
		QueueInit((Queue *) & ts[i]);

	return ts;
}

static int hash(tabSpace * ts, char *s)
{
	int hashval;

	for (hashval = 0; *s != 0;)
		hashval += *s++;

	return hashval % ts[0].hashsize;
}

tabEntry *tabLookup(tabSpace * ts, char *s)
{
	Node *np;

	for (np = ts[hash(ts, s)].q.head; np != QNULL; np = np->next)
		if (strcmp(s, ((tabEntry *) np)->name) == 0)
			return (tabEntry *) np;

	return (tabEntry *) QNULL;
}

tabEntry *tabInstall(tabSpace * ts, char *name, char *def)
{
	tabEntry *np;
	int hashval;

	if ((np = tabLookup(ts, name)) == QNULL) {
		if (np = (tabEntry *) malloc(sizeof (*np))) {
			if (np->name = strdup(name))
				if (np->def = strdup(def)) {
					hashval = hash(ts, name);
					InsertTail((Queue *) & ts[hashval], (Node *) np);
				}
				else {
					free(np->name);
					free(np);
				}
			else
				free(np);
		}
	}
	else {
		if (np->def)
			free(np->def);
		np->def = strdup(def);
	}
	return np;
}
