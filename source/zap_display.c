static char *SCCSID = "@(#)zap_display.c	1.2 07/24/95";
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void zap_display(
char *buf,			/* array of character to display to the screen */
int bufsize,		/* the size of the array */
int cpl,			/* characters per line = 16 or 64 */
long offset,		/* how far into the file are we ? */
int (*xprintf) ())	/* printf function to use */
{
	int size;
	char hex[50], ascii[66];
	char *buffer[2];
	int i, j, index = 0, dups = 0;
	unsigned long pos = 0;

	for (pos = 0; pos < bufsize; pos += cpl, index ^= 1) {
		buffer[index] = &buf[pos];
		if (pos && memcmp(buffer[index], buffer[index ^ 1], cpl) == 0) {

			if (!dups)
				xprintf("  ******\n");
			dups = 1;
			continue;
		} else
			dups = 0;

		memset(hex, ' ', sizeof(hex));
		memset(ascii, ' ', sizeof(ascii));
		for (i = 0, j = 0; i < cpl && pos + i < bufsize; i++, j += 3) {
			ascii[i] = buffer[index][i];
			if (cpl == 16)
				sprintf(&hex[j], "%02x ", (unsigned char) ascii[
										i]);
			if (buffer[index][i] < ' ' || buffer[index][i] >= 0x7f)
				ascii[i] = '.';
		}

		hex[sizeof(hex)] = ascii[cpl] = 0;
		if (cpl == 16)
			xprintf("% 8x   %-47.47s   %s\n", offset + pos, hex, ascii);
		else
			xprintf("% 8x %s\n", offset + pos, ascii);
	}
}
